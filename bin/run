#!/usr/bin/env node

const http = require("http");
const path = require("path");
const dotenv = require("dotenv");
const {
  registerService,
  serviceHeartbeat,
  serviceCleanup,
} = require("../service/lib/ServiceRegistry");

/* Setting Environment Config */
const env_path = path.join(
  __dirname,
  process.env.NODE_ENV === "production"
    ? "../.env.production"
    : "../.env.development"
);
dotenv.config({ path: env_path });

const config = require("../config")[process.env.NODE_ENV || "development"];
const log = config.log();

const service = require("../service")(config);
const server = http.createServer(service);

server.listen(process.env.PORT, async () => {
  const { name, version } = config;
  const port = `${server.address().port}`;

  await registerService({ name, version, port });
  const interval = await serviceHeartbeat({ name, version, port }, 20 * 1000);

  process.on("uncaughtException", async () => {
    await serviceCleanup({ name, version, port }, interval);
    process.exit(0);
  });

  process.on("SIGINT", async () => {
    await serviceCleanup({ name, version, port }, interval);
    process.exit(0);
  });

  process.on("SIGTERM", async () => {
    await serviceCleanup({ name, version, port }, interval);
    process.exit(0);
  });

  log.info(
    `User service listening on port ${server.address().port} in ${
      process.env.NODE_ENV || "development"
    } mode`
  );
});
