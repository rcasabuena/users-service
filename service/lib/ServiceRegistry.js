const axios = require("axios");

const registerService = async ({ name, version, port }) => {
  const data = JSON.stringify({
    name,
    version,
    port,
  });

  const config = {
    method: "put",
    url: `${process.env.SERVICE_REGISTRATION_HOST}/register`,
    headers: {
      "Service-Registration-Key": process.env.SERVICE_REGISTRATION_KEY,
      "Content-Type": "application/json",
    },
    data,
  };

  await axios(config);
};
const unregisterService = async ({ name, version, port }) => {
  const data = JSON.stringify({
    name,
    version,
    port,
  });

  var config = {
    method: "delete",
    url: `${process.env.SERVICE_REGISTRATION_HOST}/register`,
    headers: {
      "Service-Registration-Key": process.env.SERVICE_REGISTRATION_KEY,
      "Content-Type": "application/json",
    },
    data,
  };

  await axios(config);
};
const serviceHeartbeat = async ({ name, version, port }, skip) => {
  const interval = setInterval(
    () => registerService({ name, version, port }),
    skip
  );
  return interval;
};
const serviceCleanup = async ({ name, version, port }, interval) => {
  clearInterval(interval);
  await unregisterService({ name, version, port });
};

module.exports = {
  registerService,
  unregisterService,
  serviceHeartbeat,
  serviceCleanup,
};
