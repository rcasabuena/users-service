const express = require("express");
const router = express.Router();

module.exports = () => {
  router.get("/", (req, res, next) => {
    res.status(200).json({ message: "Get all users" });
  });

  router.post("/", (req, res, next) => {
    res.status(200).json({ message: "Post a new user" });
  });

  router.get("/:id", (req, res, next) => {
    res.status(200).json({ message: "Get a user by id" });
  });

  router.patch("/:id", (req, res, next) => {
    res.status(200).json({ message: "Update a user by id" });
  });

  router.delete("/:id", (req, res, next) => {
    res.status(200).json({ message: "Delete a user by id" });
  });

  return router;
};
