const express = require("express");
const routes = require("./routes");

const service = express();

module.exports = (config) => {
  service.use(express.json());

  service.use("/", routes());

  return service;
};
